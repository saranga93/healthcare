

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';
import { NewUserComponent } from './newuser.component';
const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    data: {
      title: 'admin'
    }
  },
  {
    path: 'newuser',
    component: NewUserComponent,
    data: {
      title: 'newuser'
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
