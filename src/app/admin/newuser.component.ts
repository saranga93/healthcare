import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.scss']
})
export class NewUserComponent implements OnInit {

  userForm
  role = [
    {'name':'Admin','id':1,},{'name':'Doctor','id':2},{'name':'Nurse','id':3} 
     ];
  password
  confirmPassword
  matchPassword

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.userForm = this.formBuilder.group({
      'lastName': ['', Validators.required],
      'firstName': ['', Validators.required],
      'userName': ['', Validators.required],
      'email': ['', Validators.required],
      'password': [''],
      'confirmPassword': [''],
      'phoneNumber': [''],
      'jobType': [''],
      'notes': [''],
      'description': ['', Validators.required],
      'status': [''],
      
     
   });

   }

  ngOnInit() {
  }
  passwordChecking() {
    if (this.password == this.confirmPassword) {
      this.matchPassword = false
    }else{
      this.matchPassword = true
    }

  }

  addEmployeeRegistationDetail(userForm){
    
    let userPasswordDetail= {
      password: this.password,
      confirmPassword : this.confirmPassword
    }

    let newuserDetail = {
      newuserForm: this.userForm.value,
      userPasword:userPasswordDetail
    }
    console.log('gfg',newuserDetail)
  }

 
 
}

