

import { NgModule } from '@angular/core';
import {AdminComponent } from './admin.component';
import { AdminRoutingModule} from './admin-routing.module';
import {NewUserComponent} from './newuser.component'
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common'; 
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
@NgModule({
  imports: [
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    Ng2FilterPipeModule,
    BsDropdownModule
  ],
  declarations: [AdminComponent,NewUserComponent,]
})
export class AdminModule { }
