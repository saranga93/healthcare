

import { NgModule } from '@angular/core';
import { OperationComponent } from './operation.component';
import { OperationRoutingModule} from './operation-routing.module';

import { CommonModule } from '@angular/common'; 
@NgModule({
  imports: [
    OperationRoutingModule,

  ],
  declarations: [OperationComponent ],
  providers:[
    OperationComponent 
],
})
export class OperationModule { }
