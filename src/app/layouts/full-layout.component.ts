import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router,NavigationEnd} from '@angular/router';
import "rxjs/add/operator/filter";
import "rxjs/add/operator/map";


@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.css'],
})
export class FullLayoutComponent implements OnInit {

  public disabled = false;
  public status: {isopen: boolean} = {isopen: false};
  public url=''

  constructor(private activatedRoute: ActivatedRoute, private router: Router){
    router.events.subscribe(event => {
      
            if (event instanceof NavigationEnd ) {
              console.log("current url",event.url); 
              this.url=event.url;
            }
          });
  }

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  ngOnInit() {

  }
}
