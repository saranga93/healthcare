

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CompanyComponent} from './company.component';
import {OrganizationComponent} from './organization.component';
import {NewlocationComponent} from './newlocation.component';

const routes: Routes = [
  {
    path: '',
    component:OrganizationComponent,
    data: {
      title: 'org'
    }
    },
    {
      path: 'company',
      component: CompanyComponent,
      data: {
        title: 'org'
      }
    },
    {
      path: 'newlocation',
      component: NewlocationComponent,
      data: {
        title: 'org'
      }
    }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationRoutingModule {}
