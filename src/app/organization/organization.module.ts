import { NgModule } from '@angular/core';
import {OrganizationRoutingModule } from './organization-routing.module';
import {OrganizationComponent} from './organization.component';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common'; 
import {NewlocationComponent} from './newlocation.component';
import {CompanyComponent} from './company.component'
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
@NgModule({
  imports: [
    OrganizationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    Ng2FilterPipeModule,
    BsDropdownModule,
   

  ],
  declarations: [OrganizationComponent, CompanyComponent,NewlocationComponent,]
})
export class OrganizationModule { }
