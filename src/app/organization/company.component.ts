import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
userForm
  constructor(private formBuilder: FormBuilder) {
    this.userForm = this.formBuilder.group({
        'entityName': ['', Validators.required],
        'dba': ['', Validators.required],
        'entityType': [''],
        'stateIncorparated': [''],
        'fein': [''],
        'fiscalYear': [''],
        'phoneNumber': [''],
        'fax': [''],
        'streetAddress': [''],
        'city': [''],
        'state': [''],
        'zip': [''],
        'country': [''],
        'phone': [''],
        'website': [''],
        'identfierName': [''],
        'identifierNumber': [''],
        'insurance': [''],
        'description': ['', Validators.required],
      
        
       
     });
  
   }

  ngOnInit() {
  }

}
