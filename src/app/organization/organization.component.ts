import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {

  organizationDetail = [{'id':1,'locationName':'Admin','city':'london','zip':'Admin','streetAddress':'xxx,','state':'usa','status':'good','more':'more'},
  {'id':1,'locationName':'Admin','city':'tokyo','zip':'Admin','streetAddress':'yyyy,','state':'uk','status':'mood','more':'more'} ];
  userFilter: any = { city: '' ,state:''};
  
  constructor(  
      private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
  }

}
