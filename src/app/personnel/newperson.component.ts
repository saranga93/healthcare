import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-newperson',
  templateUrl: './newperson.component.html',
  styleUrls: ['./newperson.component.css']
})
export class NewPersonComponent implements OnInit {

   state = [
        {'name':'USA','id':1,},{'name':'UK','id':2},{'name':'SA','id':3} 
         ];
   speciality = [{'id':1,'name':'channel'}];

   taxonomy = [{'id':1,'name':'10'}];

   location = [{'id':1,'name':'usa'}];
   
   supervisor = [
    {'name':'Admin','id':1,},{'name':'Doctor','id':2},{'name':'Nurse','id':3} 
     ];
     userForm
    

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.userForm = this.formBuilder.group({
        'lastName': ['', Validators.required],
        'firstName': ['', Validators.required],
        'birthday': [''],
        'socialSecurity': [''],
        'gender': [''],
        'streetAddress': [''],
        'city': [''],
        'state': [''],
        'zip': [''],
        'country': [''],
        'phoneNumber': [''],
        'email': [''],
        'jobTitle': [''],
        'location': [''],
        'description': [''],
        'status': [''],
        'supervise': [''],
        'title': [''],
        'npi': ['', Validators.required],
        'credential':['', Validators.required],
         'Speciality': [''],
         'taxonomy':['', Validators.required],
       
        
        
       
     });

   }

  ngOnInit() {
  }
 
 
 
}

