import { NgModule } from '@angular/core';
import {PersonnellRoutingModule } from './personnel-routing.module';
import {PersonnelComponent} from './personnel.component';
import { FormBuilder,  FormsModule,ReactiveFormsModule,Validators } from '@angular/forms';
import { CommonModule } from '@angular/common'; 
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import{NewPersonComponent} from './newperson.component'
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
@NgModule({
  imports: [
    PersonnellRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BsDropdownModule,
    Ng2FilterPipeModule
  ],
  declarations: [PersonnelComponent,NewPersonComponent],
})
export class PersonnelModule { }
