

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonnelComponent} from './personnel.component';
import {NewPersonComponent} from './newperson.component'
const routes: Routes = [
  {
    path: '',
    component: PersonnelComponent,
    data: {
      title: 'personnel'
    }
  },
  {
    path: 'newperson',
    component:NewPersonComponent,
    data: {
      title: 'newperson'
    }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonnellRoutingModule {}
