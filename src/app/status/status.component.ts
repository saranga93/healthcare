import { Component, OnInit } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser'

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {
    operation = true;
    addministaration;
    personnel;
    
    dataTable= [
      {'id':1,'lastname':'cfdmin','firstname':'saranga','eventObject':'saranga','status':'member','dueDate':'doctor','more':'More'} ,
      {'id':1,'lastname':'cfdmin','firstname':'saranga','eventObject':'saranga','status':'member','dueDate':'doctor','more':'More'} ,
      {'id':1,'lastname':'cfdmin','firstname':'saranga','eventObject':'saranga','status':'member','dueDate':'doctor','more':'More'} ,
       ];
 
    operationColor: string = '#E4E5E7'; 
    personColor: string = '	#E4E5E7'; 
    administrationColor: string = '	#E4E5E7'; 
    fontColor: string = '#000000'; 
    fontdisplayColor  = '#000000'; 
    adddminFontColor  = '#000000'; 
  constructor() {
    
   }

  ngOnInit() {
    this.operation = true;
    this.operationColor = '#E3570D'; 
    this.fontColor ='#ffff';
  }

  displayOperation(){

    this.operation = true;
    this.addministaration =false;
    this.personnel = false;
    this.operationColor = '#E3570D'; 
    this.personColor = '	#E4E5E7'; 
    this.administrationColor = '#E4E5E7'; 
    this.fontColor ='#ffff';
    this.fontdisplayColor = '#000000';
    this.adddminFontColor= '#000000';

   
    
  }
  displayPersonel(){
   
    this.operation = false;
    this.addministaration =false;
    this.personnel = true;
    this.personColor = '#0A8B00'; 
    this.operationColor = '#E4E5E7'; 
    this.fontColor = "#000000"
    this.adddminFontColor= '#000000';
    this.administrationColor = '#E4E5E7';
    this.fontdisplayColor = '#ffff';
  }
  displayAdministration(){
    this.operation = false;
    this.addministaration =true;
    this.personnel = false;
 
    this.administrationColor = '#D74994';
    this.operationColor = '#E4E5E7'; 
    this.personColor = '#E4E5E7'; 
    this.adddminFontColor= '#ffff';
    this.fontdisplayColor = '#000000';
    this.fontColor ='#000000';
  }
}
