

import { NgModule } from '@angular/core';
import { StatusRoutingModule } from './status-routing.module';
import { StatusComponent} from './status.component';
import { CommonModule } from '@angular/common';
import { FormBuilder,  FormsModule,ReactiveFormsModule,Validators } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
  imports: [
    StatusRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    BsDropdownModule,
  ],
  declarations: [StatusComponent]
})
export class StatusModule { }
