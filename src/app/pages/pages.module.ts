import { NgModule } from '@angular/core';


import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';
import { FormsModule, ReactiveFormsModule, } from '@angular/forms';
import { PagesRoutingModule } from './pages-routing.module';


@NgModule({
  imports: [  FormsModule,PagesRoutingModule,],
  declarations: [
 
    LoginComponent,
    RegisterComponent,
  ]
})
export class PagesModule { }
